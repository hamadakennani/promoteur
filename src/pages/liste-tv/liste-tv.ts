import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { DetailTransactionTvPage } from '../detail-transaction-tv/detail-transaction-tv';

/**
 * Generated class for the ListeTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-liste-tv',
  templateUrl: 'liste-tv.html',
})
export class ListeTvPage {
  listeTV : any[];
  montantTranscations : number;
  type : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global: GlobalProvider,public loading: LoadingController,private alertCtrl:AlertController) {
    this.type = this.navParams.get("type");
    
    this.ListeTicket();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListeTvPage');

  }

  goToPage(id,num_serie,montant,state){
    this.navCtrl.push(DetailTransactionTvPage,{
      "id":id,
      "num_serie":num_serie,
      "type":this.type,
      "montant" :montant,
      "state":state
    })
  }

 ListeTicket(){
  let loading = this.loading.create({content : "Logging in ,please wait..."});
  loading.present();
  this.http.get(this.global.url+'/getListeTicketByStation.php?station_id='+this.global.point_vente+'&&type='+this.type)
  .map(res => res.json())
  .subscribe(res => {

    this.listeTV = res; 
    this.http.get(this.global.url+'/getMontant.php?station_id='+this.global.point_vente+'&&type='+this.type)
    .map(res => res.json())
    .subscribe(res => {
      this.montantTranscations = res.montant;
      loading.dismissAll();
    }, (err) => {        
      console.log(err);
      loading.dismissAll();
    }); 
  }, (err) => {
                
    console.log(err);
    loading.dismissAll();
  });
 }

 getMontant(){
  
 }

}
