import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailTransactionTvPage } from './detail-transaction-tv';

@NgModule({
  declarations: [
    DetailTransactionTvPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailTransactionTvPage),
  ],
})
export class DetailTransactionTvPageModule {}
