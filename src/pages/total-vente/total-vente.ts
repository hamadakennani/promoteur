import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';

/**
 * Generated class for the TotalVentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-total-vente',
  templateUrl: 'total-vente.html',
})
export class TotalVentePage {
  public date_paiment:string;
  public liste : any[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global: GlobalProvider,public loading: LoadingController,private alertCtrl:AlertController) {

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TotalVentePage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  chercher(){
    this.getTotal();
  }


  getTotal(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.http.get(this.global.url+'/getListeMontant.php?station_id='+this.global.point_vente+'&&date='+this.date_paiment)
    .map(res => res.json())
    .subscribe(res => {
        
        this.liste = res;
        
        loading.dismissAll();
    }, (err) => {

      console.log(err);
      loading.dismissAll();
    });
  }

}
