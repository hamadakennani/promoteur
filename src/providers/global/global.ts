import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, App } from 'ionic-angular';
import { AuthentificationPage } from '../../pages/authentification/authentification';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  public url : string = 'http://147.135.157.64/api_tv';
  public tv_id   : string;
  public client  : string;
  public MontantTV  : string;
  public num_serie  : string;
  public point_vente:string;
  public client_carte  : string;
  public MontantCarte  : string;
  public carte_id:string;
  constructor(public http: HttpClient,private alertCtrl:AlertController,private app:App) {
    console.log('Hello GlobalProvider Provider');
  }



  presentAlert(title,text,type,page,root) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [{
        text: 'ok',
        role: 'ok',
        handler: () => {
          if(type == "success"){
            root.navCtrl.setRoot(page);
          }
        }
      }],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  logout() {
    //this.navCtrl.push(AuthentifiactionPage);
    let alert = this.alertCtrl.create({
      title: 'Fermeture de la session',
      message: 'voulez-vous vraiment quitter la session ?',
      buttons: [
        {
          text: 'Confirmer',
          handler: () => {
            console.log('Buy clicked');
            //root.setRoot(AuthentifiactionPage);
            this.app.getRootNav().setRoot(AuthentificationPage);
          }
        },
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
    
    
  } 

}
