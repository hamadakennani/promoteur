webpackJsonp([11],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CodebarrePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detail_tv_detail_tv__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CodebarrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CodebarrePage = /** @class */ (function () {
    function CodebarrePage(navCtrl, navParams, http, global, loading, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.codebarre = "";
    }
    CodebarrePage_1 = CodebarrePage;
    CodebarrePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CodebarrePage');
    };
    CodebarrePage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    CodebarrePage.prototype.valider = function () {
        var _this = this;
        if (this.codebarre != "") {
            var loading_1 = this.loading.create({ content: "Logging in ,please wait..." });
            loading_1.present();
            this.http.get(this.global.url + '/existTicket.php?qrcode=' + this.codebarre)
                .map(function (res) { return res.json(); })
                .subscribe(function (res) {
                if (res == 0) {
                    loading_1.dismissAll();
                    _this.global.presentAlert("message", "Ticket inexistante", "ioio", CodebarrePage_1, _this);
                }
                else {
                    _this.http.get(_this.global.url + '/getInfoParTicket.php?qrcode=' + _this.codebarre)
                        .map(function (res) { return res.json(); })
                        .subscribe(function (res) {
                        //this.global.presentAlert("message",res.client,"success",VentePage,this);
                        if (res[0].etat == 'util') {
                            _this.global.presentAlert("Attention", "Ce ticket a été dèjà utilisé dans le passé", "ioio", CodebarrePage_1, _this);
                            loading_1.dismissAll();
                        }
                        else {
                            _this.global.tv_id = res[0].id;
                            _this.global.client = res[0].client;
                            _this.global.MontantTV = res[0].montant;
                            _this.global.num_serie = res[0].num_serie;
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__detail_tv_detail_tv__["a" /* DetailTvPage */], {
                                'type_vente': 'TV'
                            });
                            loading_1.dismissAll();
                        }
                    }, function (err) {
                        console.log(err);
                        loading_1.dismissAll();
                    });
                }
            }, function (err) {
                console.log(err);
                loading_1.dismissAll();
            });
        }
    };
    CodebarrePage = CodebarrePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-codebarre',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/codebarre/codebarre.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Numéro de code barre</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n      <div class="login-box">\n        <ion-row>\n          <ion-col>\n            <ion-list inset>\n              <ion-item style="margin-bottom: 25px;border-radius: 5px;background-color: transparent;border-bottom: 1px solid white;font-size: 19px;color: black">\n                  <ion-input [(ngModel)] = "codebarre" placeholder="Code Barre" type="text"></ion-input>\n              </ion-item>\n              <button ion-button block style="border-radius: 5px;font-weight: bold;" (click)="valider()">Valider</button>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n    </div>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/codebarre/codebarre.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], CodebarrePage);
    return CodebarrePage;
    var CodebarrePage_1;
}());

//# sourceMappingURL=codebarre.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TypeVentePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__type_tv_type_tv__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__liste_tv_liste_tv__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__detail_tv_detail_tv__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_barcode_scanner__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the TypeVentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TypeVentePage = /** @class */ (function () {
    function TypeVentePage(navCtrl, navParams, http, global, loading, barcodeScanner) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
        this.barcodeScanner = barcodeScanner;
    }
    TypeVentePage_1 = TypeVentePage;
    TypeVentePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TypeVentePage');
    };
    TypeVentePage.prototype.verifierCode = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__type_tv_type_tv__["a" /* TypeTvPage */]);
    };
    TypeVentePage.prototype.ListeTV = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__liste_tv_liste_tv__["a" /* ListeTvPage */]);
    };
    TypeVentePage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    TypeVentePage.prototype.jnpPass = function () {
        this.ScanBarreCode();
    };
    TypeVentePage.prototype.momoPay = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__detail_tv_detail_tv__["a" /* DetailTvPage */], {
            "type_vente": "MOMO"
        });
    };
    TypeVentePage.prototype.ScanBarreCode = function () {
        var _this = this;
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        var name;
        var prix;
        var codeBarre;
        this.barcodeScanner.scan().then(function (barcodeData) {
            if (barcodeData.text != "") {
                _this.http.get(_this.global.url + '/checkCartByQrcode.php?qrcode=' + barcodeData.text)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (res) {
                    loading.dismissAll();
                    if (res == 0) {
                        _this.global.presentAlert("message", "Carte inexistante", "ioio", TypeVentePage_1, _this);
                    }
                    else {
                        _this.http.get(_this.global.url + '/getInfoParCarte.php?qrcode=' + barcodeData.text)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (res) {
                            //this.global.presentAlert("message",res.client,"success",VentePage,this);
                            _this.global.client_carte = res.client;
                            _this.global.MontantCarte = res.solde;
                            _this.global.carte_id = res.id;
                            if (res.state == "terminee") {
                                _this.global.presentAlert("Carte Perdu", "Impossible de passser des transaction avec cette carte", "fdffdf", TypeVentePage_1, _this);
                            }
                            if (res.state == "expiree") {
                                _this.global.presentAlert("Carte Expiree", "Impossible de passser des transaction avec cette carte", "fdffdf", TypeVentePage_1, _this);
                            }
                            if (res.state == "suspendu") {
                                _this.global.presentAlert("Carte Bloquée", "Impossible de passser des transaction avec cette carte", "fdffdf", TypeVentePage_1, _this);
                            }
                            if (res.state == "generee" || res.state == "activee") {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__detail_tv_detail_tv__["a" /* DetailTvPage */], {
                                    "type_vente": "CARTE"
                                });
                            }
                        }, function (err) {
                            console.log(err);
                            loading.dismissAll();
                        });
                    }
                }, function (err) {
                    console.log(err);
                    loading.dismissAll();
                });
            }
            else {
                loading.dismissAll();
            }
        }).catch(function (err) {
            //    console.log('Error', err);
            loading.dismissAll();
        });
    };
    TypeVentePage = TypeVentePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-type-vente',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/type-vente/type-vente.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Accueil</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n        <ion-list>\n            <ion-item  (click)="verifierCode()">\n                <button ion-button full  class="button_home">Ventes par TV</button>\n            </ion-item>\n            <ion-item  (click)="jnpPass()">\n                <button ion-button full  class="button_home">Ventes par JNP PASS</button>\n            </ion-item>\n            <ion-item  (click)="momoPay()">\n                <button ion-button full  class="button_home">Ventes par MOMO PAYE</button>\n            </ion-item>\n        </ion-list>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/type-vente/type-vente.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], TypeVentePage);
    return TypeVentePage;
    var TypeVentePage_1;
}());

//# sourceMappingURL=type-vente.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTransactionTvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__vente_vente__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetailTransactionTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailTransactionTvPage = /** @class */ (function () {
    function DetailTransactionTvPage(navCtrl, navParams, http, global, loading, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.type = navParams.get("type");
        this.MontantTV = navParams.get("montant");
        this.tv_id = navParams.get("id");
        this.state = navParams.get("state");
        if (this.type == 'TV') {
            this.getTicketById(navParams.get("num_serie"));
        }
    }
    DetailTransactionTvPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailTransactionTvPage');
    };
    DetailTransactionTvPage.prototype.getTicketById = function (id) {
        var _this = this;
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        this.http.get(this.global.url + '/getInfoParTicket.php?qrcode=' + id)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            _this.tv_id = res[0].id;
            _this.client = res[0].client;
            _this.MontantTV = res[0].montant;
            _this.num_serie = res[0].num_serie;
            //this.state = res[0].etat;
            loading.dismissAll();
        }, function (err) {
            console.log(err);
            loading.dismissAll();
        });
    };
    DetailTransactionTvPage.prototype.annuleTV = function (num_serie) {
        var _this = this;
        alert(this.type);
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        if (this.type == 'TV') {
            this.http.get(this.global.url + '/annuleTicket.php?num_serie=' + num_serie + '&&station_id=' + this.global.point_vente + '&&type=' + this.type)
                .map(function (res) { return res.json(); })
                .subscribe(function (res) {
                _this.global.presentAlert('BRAVO', "L'oppération effectuée avec succès.", "success", __WEBPACK_IMPORTED_MODULE_4__vente_vente__["a" /* VentePage */], _this);
                loading.dismissAll();
            }, function (err) {
                console.log(err);
                loading.dismissAll();
            });
        }
        if (this.type == 'MOMO') {
            this.http.get(this.global.url + '/annuleTicket.php?num_serie=' + this.tv_id + '&&station_id=' + this.global.point_vente + '&&type=' + this.type)
                .map(function (res) { return res.json(); })
                .subscribe(function (res) {
                _this.global.presentAlert('BRAVO', "L'oppération effectuée avec succès.", "success", __WEBPACK_IMPORTED_MODULE_4__vente_vente__["a" /* VentePage */], _this);
                loading.dismissAll();
            }, function (err) {
                console.log(err);
                loading.dismissAll();
            });
        }
    };
    DetailTransactionTvPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detail-transaction-tv',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/detail-transaction-tv/detail-transaction-tv.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Détail TV</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n      <h1  *ngIf="type == \'TV\'">Information sur le TV</h1>\n      <p  *ngIf="type == \'TV\'"><strong>Nom du client : </strong>{{client}} </p>\n      <p  *ngIf="type == \'TV\'"><strong>Numéro de serie : </strong>{{num_serie}}</p>\n      <p><strong>montant:</strong> {{MontantTV}}</p>\n      <p style="display: none;"><strong>state:</strong> {{state}}</p>\n      <button ion-button block style="border-radius: 5px;font-weight: bold;" (click)="annuleTV(num_serie)" *ngIf="state != \'cancel\'">Annuler Le ticket</button>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/detail-transaction-tv/detail-transaction-tv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], DetailTransactionTvPage);
    return DetailTransactionTvPage;
}());

//# sourceMappingURL=detail-transaction-tv.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsultationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__liste_tv_liste_tv__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ConsultationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConsultationPage = /** @class */ (function () {
    function ConsultationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ConsultationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConsultationPage');
    };
    ConsultationPage.prototype.goTopage = function (type) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__liste_tv_liste_tv__["a" /* ListeTvPage */], {
            'type': type
        });
    };
    ConsultationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-consultation',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/consultation/consultation.html"*/'<ion-header>\n    <ion-navbar text-center color=\'primary\'>\n      <ion-title>Consultation</ion-title>\n      <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n      <ion-buttons end>\n          <button ion-button icon-only clear  (click)="logout()">\n              <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n          </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding class="content">\n      <ion-card-content>\n          <ion-list>\n              <ion-item  (click)="goTopage(\'TV\')">\n                  <button ion-button full  class="button_home">Ventes par TV</button>\n              </ion-item>\n              <ion-item  (click)="goTopage(\'JNP\')">\n                  <button ion-button full  class="button_home">Ventes par JNP PASS</button>\n              </ion-item>\n              <ion-item  (click)="goTopage(\'MOMO\')">\n                  <button ion-button full  class="button_home">Ventes par MOMO PAYE</button>\n              </ion-item>\n          </ion-list>\n      </ion-card-content>\n  </ion-content>\n  '/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/consultation/consultation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ConsultationPage);
    return ConsultationPage;
}());

//# sourceMappingURL=consultation.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TotalVentePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TotalVentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TotalVentePage = /** @class */ (function () {
    function TotalVentePage(navCtrl, navParams, http, global, loading, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
    }
    TotalVentePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TotalVentePage');
    };
    TotalVentePage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    TotalVentePage.prototype.chercher = function () {
        this.getTotal();
    };
    TotalVentePage.prototype.getTotal = function () {
        var _this = this;
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        this.http.get(this.global.url + '/getListeMontant.php?station_id=' + this.global.point_vente + '&&date=' + this.date_paiment)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            _this.liste = res;
            loading.dismissAll();
        }, function (err) {
            console.log(err);
            loading.dismissAll();
        });
    };
    TotalVentePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-total-vente',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/total-vente/total-vente.html"*/'<ion-header>\n    <ion-navbar text-center color=\'primary\'>\n      <ion-title>Accueil</ion-title>\n      <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n      <ion-buttons end>\n          <button ion-button icon-only clear  (click)="logout()">\n              <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n          </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding class="content">\n      <ion-card-content>\n          <ion-list>\n              <ion-item>\n                <ion-label>Date</ion-label>\n                <ion-datetime display-format="MMM DD, YYYY" [(ngModel)] = "date_paiment" ></ion-datetime>\n              </ion-item>\n              <button ion-button block style="border-radius: 5px;font-weight: bold;" (click)="chercher()">Chercher</button>\n          </ion-list>\n          \n      <ion-item  style="cursor: pointer;" *ngFor="let p of liste" >\n          <ion-label style="padding: 10px 5px;margin:0px;"><p style="color: black;font-weight: bold;"  [ngClass]="(p.type== \'Total\') ? \'annuler\' : \'activer\'">{{p.type}}</p></ion-label>\n          <ion-label style="padding: 10px 5px;margin:0px;"><p style="color: black"  [ngClass]="(p.type== \'Total\') ? \'annuler\' : \'activer\'">{{p.montant}} CFA</p></ion-label>\n      </ion-item>\n      </ion-card-content>\n  </ion-content>\n  '/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/total-vente/total-vente.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], TotalVentePage);
    return TotalVentePage;
}());

//# sourceMappingURL=total-vente.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentification_authentification__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FirstPage = /** @class */ (function () {
    function FirstPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FirstPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad FirstPage');
        var myInterval = setInterval(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__authentification_authentification__["a" /* AuthentificationPage */]);
            clearInterval(myInterval);
        }, 3000);
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-first',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/first/first.html"*/'<ion-content  class="myphoto" style="background-color: rgb(223, 223, 226)">\n\n  <img src="assets/imgs/logo_akad.png" width="250"/>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/first/first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 126:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 126;

/***/ }),

/***/ 168:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/authentification/authentification.module": [
		292,
		10
	],
	"../pages/codebarre/codebarre.module": [
		293,
		9
	],
	"../pages/consultation/consultation.module": [
		294,
		8
	],
	"../pages/detail-transaction-tv/detail-transaction-tv.module": [
		295,
		7
	],
	"../pages/detail-tv/detail-tv.module": [
		296,
		6
	],
	"../pages/first/first.module": [
		297,
		5
	],
	"../pages/liste-tv/liste-tv.module": [
		298,
		4
	],
	"../pages/total-vente/total-vente.module": [
		299,
		3
	],
	"../pages/type-tv/type-tv.module": [
		300,
		2
	],
	"../pages/type-vente/type-vente.module": [
		301,
		1
	],
	"../pages/vente/vente.module": [
		302,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 168;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_authentification_authentification__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GlobalProvider = /** @class */ (function () {
    function GlobalProvider(http, alertCtrl, app) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.url = 'http://147.135.157.64/api_tv';
        console.log('Hello GlobalProvider Provider');
    }
    GlobalProvider.prototype.presentAlert = function (title, text, type, page, root) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [{
                    text: 'ok',
                    role: 'ok',
                    handler: function () {
                        if (type == "success") {
                            root.navCtrl.setRoot(page);
                        }
                    }
                }],
            enableBackdropDismiss: false
        });
        alert.present();
    };
    GlobalProvider.prototype.logout = function () {
        var _this = this;
        //this.navCtrl.push(AuthentifiactionPage);
        var alert = this.alertCtrl.create({
            title: 'Fermeture de la session',
            message: 'voulez-vous vraiment quitter la session ?',
            buttons: [
                {
                    text: 'Confirmer',
                    handler: function () {
                        console.log('Buy clicked');
                        //root.setRoot(AuthentifiactionPage);
                        _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_authentification_authentification__["a" /* AuthentificationPage */]);
                    }
                },
                {
                    text: 'Annuler',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    GlobalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */]) === "function" && _c || Object])
    ], GlobalProvider);
    return GlobalProvider;
    var _a, _b, _c;
}());

//# sourceMappingURL=global.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-start></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h2>Welcome to Ionic!</h2>\n  <p>\n    This starter project comes with simple tabs-based layout for apps\n    that are going to primarily use a Tabbed UI.\n  </p>\n  <p>\n    Take a look at the <code>src/pages/</code> directory to add or change tabs,\n    update any existing page or create new pages.\n  </p>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(235);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_barcode_scanner__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_authentification_authentification__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_first_first__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common_http__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_vente_vente__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_detail_tv_detail_tv__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_codebarre_codebarre__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_type_tv_type_tv__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_liste_tv_liste_tv__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_detail_transaction_tv_detail_transaction_tv__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_type_vente_type_vente__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_consultation_consultation__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_total_vente_total_vente__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_11__pages_authentification_authentification__["a" /* AuthentificationPage */], __WEBPACK_IMPORTED_MODULE_12__pages_first_first__["a" /* FirstPage */], __WEBPACK_IMPORTED_MODULE_16__pages_vente_vente__["a" /* VentePage */], __WEBPACK_IMPORTED_MODULE_17__pages_detail_tv_detail_tv__["a" /* DetailTvPage */], __WEBPACK_IMPORTED_MODULE_18__pages_codebarre_codebarre__["a" /* CodebarrePage */], __WEBPACK_IMPORTED_MODULE_19__pages_type_tv_type_tv__["a" /* TypeTvPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_liste_tv_liste_tv__["a" /* ListeTvPage */], __WEBPACK_IMPORTED_MODULE_21__pages_detail_transaction_tv_detail_transaction_tv__["a" /* DetailTransactionTvPage */], __WEBPACK_IMPORTED_MODULE_22__pages_type_vente_type_vente__["a" /* TypeVentePage */], __WEBPACK_IMPORTED_MODULE_23__pages_consultation_consultation__["a" /* ConsultationPage */], __WEBPACK_IMPORTED_MODULE_24__pages_total_vente_total_vente__["a" /* TotalVentePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_15__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/authentification/authentification.module#AuthentificationPageModule', name: 'AuthentificationPage', segment: 'authentification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/codebarre/codebarre.module#CodebarrePageModule', name: 'CodebarrePage', segment: 'codebarre', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/consultation/consultation.module#ConsultationPageModule', name: 'ConsultationPage', segment: 'consultation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detail-transaction-tv/detail-transaction-tv.module#DetailTransactionTvPageModule', name: 'DetailTransactionTvPage', segment: 'detail-transaction-tv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detail-tv/detail-tv.module#DetailTvPageModule', name: 'DetailTvPage', segment: 'detail-tv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/first/first.module#FirstPageModule', name: 'FirstPage', segment: 'first', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/liste-tv/liste-tv.module#ListeTvPageModule', name: 'ListeTvPage', segment: 'liste-tv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/total-vente/total-vente.module#TotalVentePageModule', name: 'TotalVentePage', segment: 'total-vente', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/type-tv/type-tv.module#TypeTvPageModule', name: 'TypeTvPage', segment: 'type-tv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/type-vente/type-vente.module#TypeVentePageModule', name: 'TypeVentePage', segment: 'type-vente', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/vente/vente.module#VentePageModule', name: 'VentePage', segment: 'vente', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_11__pages_authentification_authentification__["a" /* AuthentificationPage */], __WEBPACK_IMPORTED_MODULE_12__pages_first_first__["a" /* FirstPage */], __WEBPACK_IMPORTED_MODULE_16__pages_vente_vente__["a" /* VentePage */], __WEBPACK_IMPORTED_MODULE_17__pages_detail_tv_detail_tv__["a" /* DetailTvPage */], __WEBPACK_IMPORTED_MODULE_18__pages_codebarre_codebarre__["a" /* CodebarrePage */], __WEBPACK_IMPORTED_MODULE_19__pages_type_tv_type_tv__["a" /* TypeTvPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_liste_tv_liste_tv__["a" /* ListeTvPage */], __WEBPACK_IMPORTED_MODULE_21__pages_detail_transaction_tv_detail_transaction_tv__["a" /* DetailTransactionTvPage */], __WEBPACK_IMPORTED_MODULE_22__pages_type_vente_type_vente__["a" /* TypeVentePage */], __WEBPACK_IMPORTED_MODULE_23__pages_consultation_consultation__["a" /* ConsultationPage */], __WEBPACK_IMPORTED_MODULE_24__pages_total_vente_total_vente__["a" /* TotalVentePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_0__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_first_first__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_first_first__["a" /* FirstPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VentePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__type_tv_type_tv__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__type_vente_type_vente__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__consultation_consultation__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__total_vente_total_vente__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the VentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VentePage = /** @class */ (function () {
    function VentePage(navCtrl, navParams, http, global, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
    }
    VentePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VentePage');
    };
    VentePage.prototype.verifierCode = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__type_tv_type_tv__["a" /* TypeTvPage */]);
    };
    VentePage.prototype.ListeTV = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__consultation_consultation__["a" /* ConsultationPage */]);
    };
    VentePage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    VentePage.prototype.goToPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__type_vente_type_vente__["a" /* TypeVentePage */]);
    };
    VentePage.prototype.TotalVente = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__total_vente_total_vente__["a" /* TotalVentePage */]);
    };
    VentePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-vente',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/vente/vente.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Accueil</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n        <ion-list>\n            <ion-item  (click)="goToPage()">\n                <button ion-button full  class="button_home">Ventes</button>\n            </ion-item>\n            <ion-item  (click)="ListeTV()">\n                <button ion-button full  class="button_home">Liste des ventes</button>\n            </ion-item>\n            <ion-item  (click)="TotalVente()">\n                <button ion-button full  class="button_home">Total par date</button>\n            </ion-item>\n        </ion-list>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/vente/vente.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], VentePage);
    return VentePage;
}());

//# sourceMappingURL=vente.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__vente_vente__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetailTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailTvPage = /** @class */ (function () {
    function DetailTvPage(navCtrl, navParams, global, http, loading, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.http = http;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.tv_id = this.global.tv_id;
        this.client = this.global.client;
        this.MontantTV = this.global.MontantTV;
        this.num_serie = this.global.num_serie;
        this.header_name = this.navParams.get("type_vente");
    }
    DetailTvPage_1 = DetailTvPage;
    DetailTvPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailTvPage');
    };
    DetailTvPage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    DetailTvPage.prototype.Save = function () {
        if (this.header_name == "TV") {
            this.saveTV(this.tv_id);
        }
        if (this.header_name == "CARTE") {
            this.saveJnp();
        }
        if (this.header_name == "MOMO") {
            this.saveMomo();
        }
    };
    DetailTvPage.prototype.saveTV = function (id) {
        var _this = this;
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        this.http.get(this.global.url + '/insertTicket.php?type=' + this.header_name + '&&ticket_id=' + id + '&&station_id=' + this.global.point_vente)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            _this.global.presentAlert('BRAVO', "L'oppération effectuée avec succès.", "success", __WEBPACK_IMPORTED_MODULE_4__vente_vente__["a" /* VentePage */], _this);
            loading.dismissAll();
        }, function (err) {
            console.log(err);
            loading.dismissAll();
        });
    };
    DetailTvPage.prototype.saveJnp = function () {
        var _this = this;
        if (this.MontantVente <= this.global.MontantCarte) {
            var loading_1 = this.loading.create({ content: "Logging in ,please wait..." });
            loading_1.present();
            this.http.get(this.global.url + '/insertTicket.php?type=' + this.header_name + '&&carte_id=' + this.global.carte_id + '&&montant=' + this.MontantVente + '&&station_id=' + this.global.point_vente)
                .map(function (res) { return res.json(); })
                .subscribe(function (res) {
                _this.global.presentAlert('BRAVO', "L'oppération effectuée avec succès.", "success", __WEBPACK_IMPORTED_MODULE_4__vente_vente__["a" /* VentePage */], _this);
                loading_1.dismissAll();
            }, function (err) {
                console.log(err);
                loading_1.dismissAll();
            });
        }
        else {
            this.global.presentAlert("Solde insuffisant", "Impossible de passer cette transaction", "ioio", DetailTvPage_1, this);
        }
    };
    DetailTvPage.prototype.saveMomo = function () {
        var _this = this;
        alert(this.MontantVente);
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        this.http.get(this.global.url + '/insertTicket.php?type=' + this.header_name + '&&montant=' + this.MontantVente + '&&station_id=' + this.global.point_vente)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            _this.global.presentAlert('BRAVO', "L'oppération effectuée avec succès.", "success", __WEBPACK_IMPORTED_MODULE_4__vente_vente__["a" /* VentePage */], _this);
            loading.dismissAll();
        }, function (err) {
            console.log(err);
            loading.dismissAll();
        });
    };
    DetailTvPage = DetailTvPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detail-tv',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/detail-tv/detail-tv.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Détail {{header_name}}</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n      <h1 *ngIf="header_name != \'MOMO\'">Information sur le TV</h1>\n      <p *ngIf="header_name != \'MOMO\'"><strong>Nom du client : </strong>{{client}} </p>\n      <p *ngIf="header_name != \'MOMO\'"><strong>Numéro de serie : </strong>{{num_serie}}</p>\n      <p *ngIf="header_name != \'MOMO\'"><strong>montant:</strong> {{MontantTV}}</p>\n      <p style="display: none;"><strong>montant:</strong> {{header_name}}</p>\n      \n      <ion-item style="margin-top: 10px;"  *ngIf="header_name != \'TV\'">\n        <ion-label color="primary" stacked>Montant</ion-label>\n        <ion-input [(ngModel)] = "MontantVente" type="number" placeholder="Montant" id="montant" [readonly]="only_read"></ion-input>\n      </ion-item>\n      <button ion-button block style="border-radius: 5px;font-weight: bold;" (click)="Save()">Enregistrer La vente</button>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/detail-tv/detail-tv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], DetailTvPage);
    return DetailTvPage;
    var DetailTvPage_1;
}());

//# sourceMappingURL=detail-tv.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthentificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__vente_vente__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AuthentificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AuthentificationPage = /** @class */ (function () {
    function AuthentificationPage(navCtrl, navParams, http, global, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
        this.check = true;
    }
    AuthentificationPage_1 = AuthentificationPage;
    AuthentificationPage.prototype.ionViewDidLoad = function () {
    };
    AuthentificationPage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    AuthentificationPage.prototype.login = function () {
        var _this = this;
        //this.navCtrl.setRoot(VentePage);
        var loading = this.loading.create({ content: "Chargement ,Veuillez patienter..." });
        loading.present();
        var postData = new FormData();
        postData.append("matricule", this.user);
        postData.append("password", this.password);
        var hedears = new Headers();
        hedears.append("Content-Type", "application/x-www-form-urlencoded");
        hedears.append("Access-Control-Allow-Origin", "*");
        hedears.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        this.http.post(this.global.url + "/checkAgentByMatricule.php", postData)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res == 1) {
                _this.check = true;
                //this.storage.set('login', this.user);
                _this.http.get(_this.global.url + '/getAgentByMatricule.php?matricule=' + _this.user)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (res) {
                    _this.global.point_vente = res.id;
                    loading.dismissAll();
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__vente_vente__["a" /* VentePage */]);
                }, function (err) {
                    console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
                    _this.navCtrl.setRoot(AuthentificationPage_1);
                    //console.log(err);
                });
            }
            else {
                _this.check = false;
                loading.dismissAll();
            }
            console.log(res);
        }, function (err) {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            _this.global.presentAlert('Erreur', "Le serveur peut être atteint", "error", AuthentificationPage_1, _this);
            loading.dismissAll();
            //console.log(err);
        });
    };
    AuthentificationPage = AuthentificationPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-authentification',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/authentification/authentification.html"*/'<ion-content padding class="tt custom-form" >\n  <ion-row>\n      <ion-col style="text-align: center;font-weight: bold;font-size: 20px;\n      color: #0f8141;">Bienvenue au JNP WORLD</ion-col>\n  </ion-row>\n  <ion-row class="logo-row">\n      <ion-col></ion-col>\n      <ion-col >\n        <img src="assets/imgs/logo_akad.png" width="200" />\n      </ion-col>\n      <ion-col></ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col *ngIf="!check" style="text-align: center;font-weight: bold;color:red;">Nom d\'utilisateur ou mot de passe incorrect</ion-col>\n    </ion-row>\n    <div class="login-box">\n        <ion-row>\n          <ion-col>\n            <ion-list inset>\n              <ion-item style="margin-bottom: 25px;border-radius: 5px;background-color: transparent;border-bottom: 1px solid white;font-size: 19px;color: black">\n                  <ion-input [(ngModel)] = "user" placeholder="Nom d\'utilisateur" type="text"></ion-input>\n              </ion-item>\n              <ion-item style="margin-bottom: 25px;border-radius: 5px;background-color: transparent;border-bottom: 1px solid white;font-size: 19px;color: black">\n                  <ion-input [(ngModel)] = "password" placeholder="Mot de passe" type="tel"  style="-webkit-text-security:disc;"></ion-input>\n              </ion-item>\n  \n                  <button ion-button block style="border-radius: 5px;font-weight: bold;" (click)="login()">Se connecter</button>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/authentification/authentification.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], AuthentificationPage);
    return AuthentificationPage;
    var AuthentificationPage_1;
}());

//# sourceMappingURL=authentification.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TypeTvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__codebarre_codebarre__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_tv_detail_tv__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_barcode_scanner__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the TypeTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TypeTvPage = /** @class */ (function () {
    function TypeTvPage(navCtrl, navParams, barcodeScanner, http, loading, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcodeScanner = barcodeScanner;
        this.http = http;
        this.loading = loading;
        this.global = global;
    }
    TypeTvPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TypeTvPage');
    };
    TypeTvPage.prototype.logout = function () {
        this.global.logout();
        //this.presentConfirm();
    };
    TypeTvPage.prototype.goToPage = function (type) {
        if (type == "scanner") {
            this.barcodeScanner.scan().then(function (barcodeData) {
                console.log('Barcode data', barcodeData);
            }).catch(function (err) {
                console.log('Error', err);
            });
            //this.ScanBarreCodeTV();
            //this.navCtrl.push(DetailTvPage);
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__codebarre_codebarre__["a" /* CodebarrePage */]);
        }
    };
    TypeTvPage.prototype.ScanBarreCodeTV = function () {
        var _this = this;
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        var name;
        var prix;
        var codeBarre;
        this.barcodeScanner.scan().then(function (barcodeData) {
            if (barcodeData.text != "") {
                var loading_1 = _this.loading.create({ content: "Logging in ,please wait..." });
                loading_1.present();
                _this.http.get(_this.global.url + '/existTicket.php?qrcode=' + barcodeData.text)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (res) {
                    if (res == 0) {
                        loading_1.dismissAll();
                        _this.global.presentAlert("message", "Ticket inexistante", "ioio", __WEBPACK_IMPORTED_MODULE_2__codebarre_codebarre__["a" /* CodebarrePage */], _this);
                    }
                    else {
                        _this.http.get(_this.global.url + '/getInfoParTicket.php?qrcode=' + barcodeData.text)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (res) {
                            //this.global.presentAlert("message",res.client,"success",VentePage,this);
                            if (res[0].etat == 'util') {
                                _this.global.presentAlert("Attention", "Ce ticket a été dèjà utilisé dans le passé", "ioio", __WEBPACK_IMPORTED_MODULE_2__codebarre_codebarre__["a" /* CodebarrePage */], _this);
                                loading_1.dismissAll();
                            }
                            else {
                                _this.global.tv_id = res[0].id;
                                _this.global.client = res[0].client;
                                _this.global.MontantTV = res[0].montant;
                                _this.global.num_serie = res[0].num_serie;
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_tv_detail_tv__["a" /* DetailTvPage */], {
                                    "type_vente": "TV"
                                });
                                loading_1.dismissAll();
                            }
                        }, function (err) {
                            console.log(err);
                            loading_1.dismissAll();
                        });
                    }
                }, function (err) {
                    console.log(err);
                    loading_1.dismissAll();
                });
            }
        }).catch(function (err) {
            //    console.log('Error', err);
            loading.dismissAll();
        });
    };
    TypeTvPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-type-tv',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/type-tv/type-tv.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Accueil</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n        <ion-list>\n            <ion-item  (click)="goToPage(\'scanner\')">\n                <button ion-button full  class="button_home">Scanner code barre</button>\n            </ion-item>\n            <ion-item (click)="goToPage(\'saisir\')">\n                <button ion-button full  class="button_home">Saisir code barre</button>\n            </ion-item>\n        </ion-list>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/type-tv/type-tv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */]])
    ], TypeTvPage);
    return TypeTvPage;
}());

//# sourceMappingURL=type-tv.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListeTvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__detail_transaction_tv_detail_transaction_tv__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ListeTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListeTvPage = /** @class */ (function () {
    function ListeTvPage(navCtrl, navParams, http, global, loading, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.type = this.navParams.get("type");
        this.ListeTicket();
    }
    ListeTvPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListeTvPage');
    };
    ListeTvPage.prototype.goToPage = function (id, num_serie, montant, state) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__detail_transaction_tv_detail_transaction_tv__["a" /* DetailTransactionTvPage */], {
            "id": id,
            "num_serie": num_serie,
            "type": this.type,
            "montant": montant,
            "state": state
        });
    };
    ListeTvPage.prototype.ListeTicket = function () {
        var _this = this;
        var loading = this.loading.create({ content: "Logging in ,please wait..." });
        loading.present();
        this.http.get(this.global.url + '/getListeTicketByStation.php?station_id=' + this.global.point_vente + '&&type=' + this.type)
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            _this.listeTV = res;
            _this.http.get(_this.global.url + '/getMontant.php?station_id=' + _this.global.point_vente + '&&type=' + _this.type)
                .map(function (res) { return res.json(); })
                .subscribe(function (res) {
                _this.montantTranscations = res.montant;
                loading.dismissAll();
            }, function (err) {
                console.log(err);
                loading.dismissAll();
            });
        }, function (err) {
            console.log(err);
            loading.dismissAll();
        });
    };
    ListeTvPage.prototype.getMontant = function () {
    };
    ListeTvPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-liste-tv',template:/*ion-inline-start:"/Users/mac/Desktop/promoteur/src/pages/liste-tv/liste-tv.html"*/'<ion-header>\n  <ion-navbar text-center color=\'primary\'>\n    <ion-title>Accueil</ion-title>\n    <button ion-button menuToggle><ion-icon name="menu"></ion-icon></button>\n    <ion-buttons end>\n        <button ion-button icon-only clear  (click)="logout()">\n            <ion-icon ios="ios-power" md="md-power" style="color: white;font-size: 25px;margin-right: 5px;font-weight: bold"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="content">\n    <ion-card-content>\n        <ion-card id="montantCarte">\n            <ion-grid>\n                <ion-row>\n                    <ion-col  style="text-align: center;color: darkgreen" class="solde_carte">Total : {{montantTranscations}}</ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-card>\n      <ion-list>\n        <ion-item style="font-weight: bold">\n              <ion-label *ngIf="type ==\'TV\'" style="width: 35%;flex: initial;" >Numéro de serie</ion-label>\n              <ion-label style="flex: initial;width: 18%;">Montant</ion-label>\n              <ion-label style="flex: initial;width: 35%;">Date d\'enregistrement</ion-label>\n        </ion-item>\n        <ion-item *ngFor="let p of listeTV" (click)="goToPage(p.id,p.numero_serie,p.montant,p.state)" style="cursor: pointer;" [ngClass]="(p.state== \'cancel\') ? \'annuler\' : \'activer\'" >\n            <ion-label *ngIf="type ==\'TV\'">{{p.numero_serie}}</ion-label>\n            <ion-label style="flex: initial;width: 20%;"><p>{{p.montant}}</p></ion-label>\n            <ion-label>{{p.date}}</ion-label>\n            <!--<button ion-button clear item-end>détail</button>-->\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/promoteur/src/pages/liste-tv/liste-tv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ListeTvPage);
    return ListeTvPage;
}());

//# sourceMappingURL=liste-tv.js.map

/***/ })

},[214]);
//# sourceMappingURL=main.js.map