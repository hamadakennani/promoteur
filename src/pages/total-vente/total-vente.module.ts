import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TotalVentePage } from './total-vente';

@NgModule({
  declarations: [
    TotalVentePage,
  ],
  imports: [
    IonicPageModule.forChild(TotalVentePage),
  ],
})
export class TotalVentePageModule {}
