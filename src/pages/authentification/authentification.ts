import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { VentePage } from '../vente/vente';

/**
 * Generated class for the AuthentificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-authentification',
  templateUrl: 'authentification.html',
})
export class AuthentificationPage {

  user : string;
  password : string;
  check : boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController) {
    

  

  
  }

  ionViewDidLoad() {
    
    
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  login(){
    //this.navCtrl.setRoot(VentePage);
    let loading = this.loading.create({content : "Chargement ,Veuillez patienter..."});
    loading.present();
    let postData = new FormData();
    postData.append("matricule",this.user);
    postData.append("password",this.password);
    var hedears = new Headers();
    hedears.append("Content-Type","application/x-www-form-urlencoded");
    hedears.append("Access-Control-Allow-Origin", "*");
    hedears.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    this.http.post(this.global.url+"/checkAgentByMatricule.php",postData)
    .map(res => res.json())
    .subscribe(res => {
      if(res == 1){
          this.check = true;
          //this.storage.set('login', this.user);
          this.http.get(this.global.url+'/getAgentByMatricule.php?matricule='+this.user)
          .map(res => res.json())
          .subscribe(res => {
              this.global.point_vente = res.id;
              loading.dismissAll();
              this.navCtrl.setRoot(VentePage);
              
          }, (err) => {
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
            this.navCtrl.setRoot(AuthentificationPage);
            //console.log(err);
          });
      }else{
          this.check = false;
          loading.dismissAll();
      }
      console.log(res);
      

    }, (err) => {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhh');
      this.global.presentAlert('Erreur',"Le serveur peut être atteint","error",AuthentificationPage,this);
      loading.dismissAll();
      //console.log(err);
    });
  }

}
