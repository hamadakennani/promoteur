import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailTvPage } from './detail-tv';

@NgModule({
  declarations: [
    DetailTvPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailTvPage),
  ],
})
export class DetailTvPageModule {}
