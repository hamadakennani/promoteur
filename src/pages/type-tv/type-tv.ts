import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CodebarrePage } from '../codebarre/codebarre';
import { DetailTvPage } from '../detail-tv/detail-tv';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
/**
 * Generated class for the TypeTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-type-tv',
  templateUrl: 'type-tv.html',
})
export class TypeTvPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private barcodeScanner: BarcodeScanner,public http: Http,public loading: LoadingController,public global: GlobalProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TypeTvPage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  goToPage(type){
    if(type == "scanner"){
      
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
    }).catch(err => {
        console.log('Error', err);
    });
      //this.ScanBarreCodeTV();
     //this.navCtrl.push(DetailTvPage);
    }else{
      this.navCtrl.push(CodebarrePage);
    }
  }

  ScanBarreCodeTV(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    var name : any
    var prix: any
    var codeBarre : any
    this.barcodeScanner.scan().then(barcodeData => {

      if(barcodeData.text != ""){
        let loading = this.loading.create({content : "Logging in ,please wait..."});
        loading.present();
        this.http.get(this.global.url+'/existTicket.php?qrcode='+barcodeData.text)
        .map(res => res.json())
        .subscribe(res => {
          if(res == 0){
              loading.dismissAll();
              this.global.presentAlert("message","Ticket inexistante","ioio",CodebarrePage,this);
          }else{
            this.http.get(this.global.url+'/getInfoParTicket.php?qrcode='+barcodeData.text)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
              if(res[0].etat == 'util'){
                this.global.presentAlert("Attention","Ce ticket a été dèjà utilisé dans le passé","ioio",CodebarrePage,this);
                loading.dismissAll();
              }else{
                this.global.tv_id  = res[0].id
                this.global.client = res[0].client;
                this.global.MontantTV = res[0].montant;
                this.global.num_serie = res[0].num_serie;
                this.navCtrl.push(DetailTvPage,{
                  "type_vente":"TV"
                });
                loading.dismissAll();
              }
            
            }, (err) => {
                
              console.log(err);
              loading.dismissAll();
            });
          
          }
  
          }, (err) => {
                  
            console.log(err);
            loading.dismissAll();
          });
              
        
      }

    }).catch(err => {
      //    console.log('Error', err);
        loading.dismissAll();
    });

    }
}
