import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { VentePage } from '../vente/vente';

/**
 * Generated class for the DetailTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-tv',
  templateUrl: 'detail-tv.html',
})
export class DetailTvPage {
  public tv_id   : string;
  public client  : string;
  public MontantTV  : string;
  public num_serie  : string;
  public header_name : string;
  public MontantVente  : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider,public http: Http,public loading: LoadingController,private alertCtrl:AlertController) {
    this.tv_id =  this.global.tv_id  
    this.client = this.global.client 
    this.MontantTV = this.global.MontantTV
    this.num_serie = this.global.num_serie;
    this.header_name = this.navParams.get("type_vente");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTvPage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }


  Save(){
    if(this.header_name == "TV"){
      this.saveTV(this.tv_id);
    }
    if(this.header_name == "CARTE"){
      this.saveJnp();
    }
    if(this.header_name == "MOMO"){
      this.saveMomo();
    }
  }
  
  saveTV(id){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.http.get(this.global.url+'/insertTicket.php?type='+this.header_name+'&&ticket_id='+id+'&&station_id='+this.global.point_vente)
    .map(res => res.json())
    .subscribe(res => {
      this.global.presentAlert('BRAVO',"L'oppération effectuée avec succès.","success",VentePage,this);
      loading.dismissAll();
    }, (err) => {
        
      console.log(err);
      loading.dismissAll();
    });
  }

  saveJnp(){
    if(this.MontantVente <= this.global.MontantCarte){
      let loading = this.loading.create({content : "Logging in ,please wait..."});
      loading.present();
      this.http.get(this.global.url+'/insertTicket.php?type='+this.header_name+'&&carte_id='+this.global.carte_id+'&&montant='+this.MontantVente+'&&station_id='+this.global.point_vente)
      .map(res => res.json())
      .subscribe(res => {
        this.global.presentAlert('BRAVO',"L'oppération effectuée avec succès.","success",VentePage,this);
        loading.dismissAll();
      }, (err) => {
          
        console.log(err);
        loading.dismissAll();
      });  
    } else{
      this.global.presentAlert("Solde insuffisant","Impossible de passer cette transaction","ioio",DetailTvPage,this);
    } 
  }

  saveMomo(){
    alert(this.MontantVente)
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.http.get(this.global.url+'/insertTicket.php?type='+this.header_name+'&&montant='+this.MontantVente+'&&station_id='+this.global.point_vente)
    .map(res => res.json())
    .subscribe(res => {
      this.global.presentAlert('BRAVO',"L'oppération effectuée avec succès.","success",VentePage,this);
      loading.dismissAll();
    }, (err) => {
        
      console.log(err);
      loading.dismissAll();
    });
  }

}
