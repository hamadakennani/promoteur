import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { VentePage } from '../vente/vente';
import { ListeTvPage } from '../liste-tv/liste-tv';

/**
 * Generated class for the DetailTransactionTvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-transaction-tv',
  templateUrl: 'detail-transaction-tv.html',
})
export class DetailTransactionTvPage {
  public tv_id   : string;
  public client  : string;
  public MontantTV  : string;
  public num_serie  : string;
  public state  : string;
  public type  : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global: GlobalProvider,public loading: LoadingController,private alertCtrl:AlertController) {
    this.type = navParams.get("type")
    this.MontantTV = navParams.get("montant")
    this.tv_id = navParams.get("id")
    this.state = navParams.get("state")
    if (this.type == 'TV'){
      this.getTicketById(navParams.get("num_serie"));
    }
    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTransactionTvPage');
  }

  getTicketById(id){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    this.http.get(this.global.url+'/getInfoParTicket.php?qrcode='+id)
            .map(res => res.json())
            .subscribe(res => {
                this.tv_id  = res[0].id
                this.client = res[0].client;
                this.MontantTV  = res[0].montant;
                this.num_serie = res[0].num_serie;
                //this.state = res[0].etat;
                loading.dismissAll();
            }, (err) => {

              console.log(err);
              loading.dismissAll();
            });
  }

  annuleTV(num_serie){
    alert(this.type)
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    if(this.type == 'TV'){
      this.http.get(this.global.url+'/annuleTicket.php?num_serie='+num_serie+'&&station_id='+this.global.point_vente+'&&type='+this.type)
      .map(res => res.json())
      .subscribe(res => {
        this.global.presentAlert('BRAVO',"L'oppération effectuée avec succès.","success",VentePage,this);
          loading.dismissAll();
      }, (err) => {

        console.log(err);
        loading.dismissAll();
      });
    }
    if(this.type == 'MOMO'){
      
      this.http.get(this.global.url+'/annuleTicket.php?num_serie='+this.tv_id+'&&station_id='+this.global.point_vente+'&&type='+this.type)
      .map(res => res.json())
      .subscribe(res => {
        this.global.presentAlert('BRAVO',"L'oppération effectuée avec succès.","success",VentePage,this);
          loading.dismissAll();
      }, (err) => {

        console.log(err);
        loading.dismissAll();
      });
    }
  }

}
