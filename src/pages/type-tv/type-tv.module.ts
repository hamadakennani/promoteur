import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeTvPage } from './type-tv';

@NgModule({
  declarations: [
    TypeTvPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeTvPage),
  ],
})
export class TypeTvPageModule {}
