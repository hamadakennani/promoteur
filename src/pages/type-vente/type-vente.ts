import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { TypeTvPage } from '../type-tv/type-tv';
import { ListeTvPage } from '../liste-tv/liste-tv';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { DetailTvPage } from '../detail-tv/detail-tv';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the TypeVentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-type-vente',
  templateUrl: 'type-vente.html',
})
export class TypeVentePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController,private barcodeScanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TypeVentePage');
  }


  verifierCode(){
    this.navCtrl.push(TypeTvPage);
  }
  ListeTV(){
    this.navCtrl.push(ListeTvPage);
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  jnpPass(){
    this.ScanBarreCode();
  }

  momoPay(){
    this.navCtrl.push(DetailTvPage,{
      "type_vente":"MOMO"
    });
  }


  ScanBarreCode(){
    let loading = this.loading.create({content : "Logging in ,please wait..."});
    loading.present();
    var name : any
    var prix: any
    var codeBarre : any
    this.barcodeScanner.scan().then(barcodeData => {

      if(barcodeData.text != ""){
        this.http.get(this.global.url+'/checkCartByQrcode.php?qrcode='+barcodeData.text)
        .map(res => res.json())
        .subscribe(res => {
          loading.dismissAll();
          if(res == 0){
              this.global.presentAlert("message","Carte inexistante","ioio",TypeVentePage,this);
          }else{
            this.http.get(this.global.url+'/getInfoParCarte.php?qrcode='+barcodeData.text)
            .map(res => res.json())
            .subscribe(res => {
              //this.global.presentAlert("message",res.client,"success",VentePage,this);
                this.global.client_carte = res.client;
                this.global.MontantCarte = res.solde;
                this.global.carte_id = res.id;
                if(res.state == "terminee"){
                    this.global.presentAlert("Carte Perdu","Impossible de passser des transaction avec cette carte","fdffdf",TypeVentePage,this);
                }if(res.state == "expiree"){
                  this.global.presentAlert("Carte Expiree","Impossible de passser des transaction avec cette carte","fdffdf",TypeVentePage,this);
                }if(res.state ==  "suspendu"){
                  this.global.presentAlert("Carte Bloquée","Impossible de passser des transaction avec cette carte","fdffdf",TypeVentePage,this);
                }if(res.state == "generee" || res.state == "activee"){
                  this.navCtrl.push(DetailTvPage,{
                    "type_vente":"CARTE"
                  });
                }
                
            }, (err) => {
              
              console.log(err);
              loading.dismissAll();
            });
          }
        }, (err) => {
          
          console.log(err);
          loading.dismissAll();
        });
      }else{
        loading.dismissAll();
      }
    
      
      }).catch(err => {
      //    console.log('Error', err);
        loading.dismissAll();
      });

}

}
