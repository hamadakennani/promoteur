import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { DetailTvPage } from '../detail-tv/detail-tv';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import 'rxjs/add/operator/map'
/**
 * Generated class for the CodebarrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-codebarre',
  templateUrl: 'codebarre.html',
})
export class CodebarrePage {
  public codebarre:string="";
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global: GlobalProvider,public loading: LoadingController,private alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CodebarrePage');
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  valider(){
    if(this.codebarre != ""){
      let loading = this.loading.create({content : "Logging in ,please wait..."});
      loading.present();
      this.http.get(this.global.url+'/existTicket.php?qrcode='+this.codebarre)
      .map(res => res.json())
      .subscribe(res => {
        if(res == 0){
            loading.dismissAll();
            this.global.presentAlert("message","Ticket inexistante","ioio",CodebarrePage,this);
        }else{
          this.http.get(this.global.url+'/getInfoParTicket.php?qrcode='+this.codebarre)
          .map(res => res.json())
          .subscribe(res => {
            //this.global.presentAlert("message",res.client,"success",VentePage,this);
            if(res[0].etat == 'util'){
              this.global.presentAlert("Attention","Ce ticket a été dèjà utilisé dans le passé","ioio",CodebarrePage,this);
              loading.dismissAll();
            }else{
              this.global.tv_id  = res[0].id
              this.global.client = res[0].client;
              this.global.MontantTV = res[0].montant;
              this.global.num_serie = res[0].num_serie;
              this.navCtrl.push(DetailTvPage,{
                'type_vente':'TV'
              });
              loading.dismissAll();
            }
          
          }, (err) => {
              
            console.log(err);
            loading.dismissAll();
          });
        
        }

        }, (err) => {
                
          console.log(err);
          loading.dismissAll();
        });
            
      
    }
}

}
