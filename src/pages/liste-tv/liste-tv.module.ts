import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListeTvPage } from './liste-tv';

@NgModule({
  declarations: [
    ListeTvPage,
  ],
  imports: [
    IonicPageModule.forChild(ListeTvPage),
  ],
})
export class ListeTvPageModule {}
