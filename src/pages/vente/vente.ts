import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailTvPage } from '../detail-tv/detail-tv';
import { TypeTvPage } from '../type-tv/type-tv';
import { GlobalProvider } from '../../providers/global/global';
import { ListeTvPage } from '../liste-tv/liste-tv';
import { TypeVentePage } from '../type-vente/type-vente';
import { Http } from '@angular/http';
import { ConsultationPage } from '../consultation/consultation';
import { TotalVentePage } from '../total-vente/total-vente';

/**
 * Generated class for the VentePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vente',
  templateUrl: 'vente.html',
})
export class VentePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,public global:GlobalProvider,public loading: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VentePage');
  }

  verifierCode(){
    this.navCtrl.push(TypeTvPage);
  }
  ListeTV(){
    this.navCtrl.push(ConsultationPage);
  }

  logout(){
    this.global.logout();
   //this.presentConfirm();
  }

  goToPage(){
    this.navCtrl.push(TypeVentePage);
  }

  TotalVente(){
    this.navCtrl.push(TotalVentePage);
  }

}
