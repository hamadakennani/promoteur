import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodebarrePage } from './codebarre';

@NgModule({
  declarations: [
    CodebarrePage,
  ],
  imports: [
    IonicPageModule.forChild(CodebarrePage),
  ],
})
export class CodebarrePageModule {}
