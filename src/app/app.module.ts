import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthentificationPage } from '../pages/authentification/authentification';
import { FirstPage } from '../pages/first/first';
import { GlobalProvider } from '../providers/global/global';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { VentePage } from '../pages/vente/vente';
import { DetailTvPage } from '../pages/detail-tv/detail-tv';
import { CodebarrePage } from '../pages/codebarre/codebarre';
import { TypeTvPage } from '../pages/type-tv/type-tv';
import { IonicStorageModule } from '@ionic/storage';
import { ListeTvPage } from '../pages/liste-tv/liste-tv';
import { DetailTransactionTvPage } from '../pages/detail-transaction-tv/detail-transaction-tv';
import { TypeVentePage } from '../pages/type-vente/type-vente';
import { ConsultationPage } from '../pages/consultation/consultation';
import { TotalVentePage } from '../pages/total-vente/total-vente';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,AuthentificationPage,FirstPage,VentePage,DetailTvPage,CodebarrePage,TypeTvPage,
    ListeTvPage,DetailTransactionTvPage,TypeVentePage,ConsultationPage,TotalVentePage
  ],
  imports: [
    BrowserModule,HttpModule,HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,AuthentificationPage,FirstPage,VentePage,DetailTvPage,CodebarrePage,TypeTvPage,
    ListeTvPage,DetailTransactionTvPage,TypeVentePage,ConsultationPage,TotalVentePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,BarcodeScanner
  ]
})
export class AppModule {}
